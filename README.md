# QuantiFarm Common Semantic Model

## Documentation
The documenation of the ontology (based on the `main` branch) is automatically generated and available at https://quantifarm.gitlab.io/qcsm/.

## Serialization
The ontology is available in TTL and JSON-LD formats at respectively https://quantifarm.gitlab.io/qcsm/serializations/qcsm.ttl and https://quantifarm.gitlab.io/qcsm/serializations/qcsm.jsonld.
